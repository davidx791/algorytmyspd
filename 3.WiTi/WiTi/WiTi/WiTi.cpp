﻿// WiTi.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cmath>		
#include <iostream>		
#include <fstream>		
#include <string>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <ctime>
#include <Windows.h>


#define MAX 99999999	// Duza liczba do porownan
#define BASE 2	//Baza do potegowania
#define N 11 //liczba plików

using namespace std;

class Work {//klasa przechowujaca informacje o jednym konkretnym zadaniu

private:
	int p; //czas trwania zadania
	int w; //wspolczynnik kary
	int d; //pozadany czas wykonania
	int nr; //nr zadania
	int T; //czas opoznienia

public:
	Work() { p = 0, w = 0, d = 0, nr = 0; } //konstruktor pusty
	Work(int pp, int ww, int dd, int nrr, int TT)//konstruktor z parametrami
	{
		p = pp;
		w = ww;
		d = dd;
		nr = nrr;
		T = TT;
	}

	//funkcje odpowiedzialne za odczyt danych prywatnych
	int GetP() { return p; }
	int GetW() { return w; }
	int GetD() { return d; }
	int GetNr() { return nr; }
	int GetT() { return T; }
	//funkcje odpowiedzialne za zmiane danych prywatnych
	void ChangeP(int _p) { p = _p; }
	void ChangeW(int _w) { w = _w; }
	void ChangeD(int _d) { d = _d; }
	void ChangeT(int _T) { T = _T; }
};

int *Encode(int number, int *tab) { // funkcja kodujaca liczbe w postaci binarnej do tablicy tab2
	int i = 0;
	while (number > 0) {
		if ((number & 1) == 1) {					// bitowa koniunkcja, czyli 0101 & 0001 => 0001, sprawdzamy więc, czy występuje bit na początku
			tab[i] = 1;					// jesli wystepuje, to dodajemy do listy odpowiadajacy mu numer zadania 
		}
		else
			tab[i] = 0;
		number = number >> 1;					// przesuniecie bitowe w prawo, czyli 0010 >> 1 => 0001
		i++;
	}
	return tab;
}

int Decode(int number, int *tab, int n)   //funkcja dekodujaca liczbe binarna na postac dziesietna
{
	long index = 0;
	int * tmp = new int[n]; //deklaracja tablicy pomocniczej do obliczeń

	for (int i = 0; i < n; i++) //wpisanie danych do tablicy tmp z edycją właściwego miejsca
		tmp[i] = tab[i];
	tmp[number] = 0;

	for (int i = 0; i < n; i++) //obliczenie postaci dziesietnej
		if (tmp[i] == 1)
			index += pow(BASE, i);
	delete tmp; //usunięcie tablicy

	return index;
}

int DynamicWiTi(vector<Work> allWork, int n)//Wyliczanie WiTi za pomoca programowania dynamicznego
{
	int permutation = 0;			// Zmienna okreslajaca liczbe permutacji
	permutation = pow(BASE, n) - 1;		//wyliczenie liczby permutacji
	int *tab = new int[permutation + 1];
	int *tab2 = new int[n];
	tab[0] = 0; //wartość pomocnicza gdy funkcja kodująca zwraca 0
	int sum = 0; //suma P w każdej permutacji
	for (int i = 0; i < n; i++)
		tab2[i] = 0;

	for (int i = 1; i <= permutation; i++) //właściwa pętla wykonująca się dla każdej permutacji
	{
		tab2 = Encode(i, tab2); //zakodowanie liczby binarnie w tablicy tab2
		for (int j = 0; j < n; j++)
			if (tab2[j] == 1)
				sum += allWork[j].GetP(); //wyliczenie sumy P w kazdym maxie

		tab[i] = MAX; //przypisanie maksymalnej wartości
		for (int j = 0; j < n; j++)
			if (tab2[j] == 1) //aktualizowanie wartości minimalnej każdej permutacji zgodnie z algorytmem
				tab[i] = min(tab[i], max(sum - allWork[j].GetD(), 0)*allWork[j].GetW() + tab[Decode(j, tab2, n)]);

		sum = 0; //wyzerowanie sumy
	}
	int witi = tab[permutation];
	delete tab;
	delete tab2;

	return witi;
}

int WiTi(vector<Work> allWork)//Obliczanie WiTi na podstawie aktualnego uszeregowania
{
	int size = allWork.size();
	int C = 0;
	int sum = 0;


	for (int i = 0; i < size; i++)
	{
		C += allWork[i].GetP();
		allWork[i].ChangeT(max(C - allWork[i].GetD(), 0));
		
		sum += allWork[i].GetW() * allWork[i].GetT();
		//cout << allWork[i].GetT() << "\n";
	}

	return sum;
}

void DSort(vector<Work> &allWork, int left, int right)
{
	int i = left;
	int j = right;
	Work temp;
	int pivot = allWork[(left + right) / 2].GetD();

	do {
		while (allWork[i].GetD() < pivot) i++;
		while (allWork[j].GetD() > pivot) j--;
		if (i <= j) {

			temp = allWork[i];

			allWork[i] = allWork[j];
			i++;

			allWork[j] = temp;
			j--;

		}
	} while (i <= j);

	if (left < j) DSort(allWork, left, j);
	if (right > i) DSort(allWork, i, right);
}

void _2opt(vector<Work> &allWork)
{
	int previousWiTi, presentWiTi;
	vector<Work>tempWork = allWork;
	int size = allWork.size();

	previousWiTi = WiTi(allWork);

	for (int i = 0; i < size - 1; i++)
	{

		for (int j = i + 1; j < size; j++)
		{

			tempWork[i] = allWork[j];//zamiana miejscami dwoch zadan
			tempWork[j] = allWork[i];
			presentWiTi = WiTi(tempWork);//wyliczanie CMax dla aktualnego ukladu zadan

			if (presentWiTi < previousWiTi)//jezeli po zamianie doszlo do polepszenia to zostawiamy to i startujemy o 0
			{
				previousWiTi = presentWiTi;
				allWork[i] = tempWork[i];
				allWork[j] = tempWork[j];
				i = 0;
				j = i + 1;

			}
			else
			{
				tempWork[i] = allWork[i];
				tempWork[j] = allWork[j];
			}
		}
	}
}

LARGE_INTEGER startTimer()//start odmierzania czasu
{
	LARGE_INTEGER start;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&start);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return start;
}
LARGE_INTEGER endTimer()//zakonczenie odmierzania czasu
{
	LARGE_INTEGER stop;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&stop);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return stop;
}

int main()
{
	//Zmienne do mierzenia czasu
	LARGE_INTEGER performanceCountStart, performanceCountEnd;
	__int64 freq;
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
	double WiTi_Time, DSort_Time, _2opt_Time;

	vector<Work> allWork, allWork2; //wektor wszystkich zadañ
	Work work; // pojedyncze zadanie
	int n = 0; //ilosc zadan
	int p = 0; //czas trwania zadania
	int w = 0; //wspolczynnik kary
	int d = 0; //zadany czas wykonania

	string name[N] = { "data10.txt","data11.txt","data12.txt","data13.txt","data14.txt","data15.txt","data16.txt","data17.txt","data18.txt","data19.txt","data20.txt" };
	fstream plik;

	for (int k = 0; k < N; k++)//petla wlasciwego dzialania programu
	{
		cout << "Nazwa pliku: " << name[k] << "\n";
		plik.open(name[k].c_str());
		plik >> n;

		// Wczytanie informacji o zadaniach
		for (int i = 0; i < n; i++)
		{
			plik >> p >> w >> d;
			work = Work(p, w, d, i + 1, 0);
			allWork.push_back(work);
			allWork2.push_back(work);
			//cout << allWork[i].GetP() << " " << allWork[i].GetW() << " " << allWork[i].GetD() << "\n";
		}
		cout << "\n";
		cout << "WiTi bez permutacji:" << WiTi(allWork) << "\n\n";
		
		performanceCountStart = startTimer();//start timera
		cout << "WiTi dla dynamicznego: " << DynamicWiTi(allWork, n) << "\n";
		performanceCountEnd = endTimer(); //stop timera
		WiTi_Time = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
		cout << "Czas wykonywania: " << 1000 * WiTi_Time << " ms\n\n";
		
		performanceCountStart = startTimer();//start timera
		DSort(allWork2, 0, n - 1);
		cout << "WiTi dla DSort: " << WiTi(allWork2) << "\n";
	    performanceCountEnd = endTimer(); //stop timera
		DSort_Time = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
		cout << "Czas wykonywania DSort'a: " << 1000 * DSort_Time << " ms\n\n";
		
		performanceCountStart = startTimer();//start timera
		_2opt(allWork);
		cout << "WiTi dla 2opt'a: " << WiTi(allWork) << "\n";
		performanceCountEnd = endTimer(); //stop timera
		_2opt_Time = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
		cout << "Czas wykonywania 2opt'a: " << 1000 * _2opt_Time << " ms\n";
		
		cout << "\n************************************************\n\n";

		plik.close();
		allWork.clear();
		allWork2.clear();
	}
	system("pause");
	exit(0);
}