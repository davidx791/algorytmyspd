// SA.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cmath>
#include <iostream>		
#include <fstream>		
#include <string>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <ctime>
#include <Windows.h>

#define MAX 99999999	// Duza liczba do porownan
#define BASE 2	//Baza do potegowania
#define N 120 //liczba plikow

using namespace std;

class Work {//klasa przechowujaca informacje o jednym konkretnym zadaniu

private:
	int p; //czas trwania zadania
	int nr; //nr zadania
	int nrM;//nr maszyny
	int T;//czas zakonczenia

public:
	Work() { p = 0, nr = 0, nrM = 0; T = 0; } //konstruktor pusty
	Work(int pp, int nrr, int nrrM)//konstruktor z parametrami
	{
		p = pp;
		nr = nrr;
		nrM = nrrM;
		T = 0;
	}

	//funkcje odpowiedzialne za odczyt danych prywatnych
	int GetP() { return p; }
	int GetNr() { return nr; }
	int GetNrM() { return nrM; }
	int GetT() { return T; }

	void ChangeP(int pp) { p = pp; }
	void ChangeNr(int nrr) { nr = nrr; }
	void ChangeNrM(int nrrM) { nrM = nrrM; }
	void ChangeT(int TT) { T = TT; }

};

int CMax(Work **tab, int n, int m)
{
	int CMax = 0;
	int tmpT = 0;
	int j = 0;
	for (int i = 0; i < n; i++)
	{

		if (tab[i][j].GetNrM() == 1)
		{
			if (i > 0)
				tab[i][j].ChangeT(tab[i][j].GetP() + tab[i - 1][j].GetT());
			else
				tab[i][j].ChangeT(tab[i][j].GetP());
		}
		else
		{
			if (i > 0)
			{
				tmpT = max(tab[i][j].GetP() + tab[i][j - 1].GetT(), tab[i][j].GetP() + tab[i - 1][j].GetT());
				tab[i][j].ChangeT(tmpT);
			}
			else
			{
				tmpT = max(tab[i][j].GetP() + tab[i][j - 1].GetT(), tab[i][j].GetP());
				tab[i][j].ChangeT(tmpT);
			}
		}
		//cout << tab[i][j].GetT() << " ";
		if (i == n - 1)
		{
			j++;
			i = -1;
			if (j == m)
			{
				//cout << "\n";
				break;
			}
		}
	}
	CMax = tab[n - 1][m - 1].GetT();
	return CMax;
}

void insert(Work **&tab, int index1, int index2, int n, int m)
{
	//index1 -> wybrane zadanie
	//index2 -> miejsce wstawienia
	int newIndex, newIndex2, counter;
	Work tmp;
	for (int y = 0; y < m; y++)
	{
		tmp = tab[index1][y];
		if (index1 > index2)
			counter = index1 - index2;
		else
			counter = index1 - index2 + n;

		for (int i = 0; i < counter; i++)
		{
			if (index1 - i >= 0) //sprawdzenie czy trzeba przejsc na koniec tablicy
				newIndex = index1 - i;
			else
				newIndex = index1 - i + n;

			if (index1 - i - 1 >= 0) //sprawdzenie czy trzeba przejsc na koniec tablicy
				newIndex2 = index1 - i - 1;
			else
				newIndex2 = index1 - i - 1 + n;
			tab[newIndex][y] = tab[newIndex2][y];
		}
		tab[index2][y] = tmp;
	}
}

void invInsert(Work **&tab, int index1, int index2, int n, int m)
{
	//index1 -> wybrane zadanie
	//index2 -> miejsce wstawienia
	int newIndex, newIndex2, counter;
	Work tmp;
	for (int y = 0; y < m; y++)
	{
		tmp = tab[index1][y];
		if (index1 < index2)
			counter = index2 - index1;
		else
			counter = index2 - index1 + n;

		for (int i = 0; i < counter; i++)
		{
			if (index1 + i <= n - 1) //sprawdzenie czy trzeba przejsc na koniec tablicy
				newIndex = index1 + i;
			else
				newIndex = index1 + i - n;

			if (index1 + i + 1 <= n - 1) //sprawdzenie czy trzeba przejsc na koniec tablicy
				newIndex2 = index1 + i + 1;
			else
				newIndex2 = index1 + i + 1 - n;


			tab[newIndex][y] = tab[newIndex2][y];
		}
		tab[index2][y] = tmp;
	}
}

void twist(Work **&tab, int index1, int index2, int m)
{
	int counter, pom;
	Work tmp;
	for (int y = 0; y < m; y++) //zamiana zadań
	{
		if (abs(index1 - index2) == 1)
			counter = 1;
		else
			counter = floor(abs(index1 - index2) / 2);

		if (index1 > index2)
		{
			pom = index1;
			index1 = index2;
			index2 = pom;
		}

		for (int i = 0; i < counter; i++)
		{
			tmp = tab[index1 + i][y];
			tab[index1 + i][y] = tab[index2 - i][y];
			tab[index2 - i][y] = tmp;
		}
	}
}

void swap(Work **&tab, int index1, int index2, int m)
{

	Work tmp;
	//zamiana zadań dla wszystkich maszyn
	for (int y = 0; y < m; y++) 
	{
		tmp = tab[index1][y];
		tab[index1][y] = tab[index2][y];
		tab[index2][y] = tmp;
	}
}

int SA(Work **&tab, Work **&tabTmp, int n, int m, int firstCMax, int mode)
{
	float Temp = 1000.0, TempMin = 0.001; //temperatura poczatkowa
	float wsp = 0.8;
	float rnd;
	int currentCMax = 0;
	int previousCMax = firstCMax;

	int deltaCMax = 0;
	float p = 0;

	while (Temp > TempMin)
	{
		for (int i = 0; i < 1000; i++)
		{
			int index1 = rand() % n;
			int index2 = rand() % n;
			if (index1 == index2)
			{
				index2++;
				if (index2 == n)
					index2 = 0;
			}

			/*losowy ruch*/
			if (mode == 1)
				swap(tabTmp, index1, index2, m); //zamiana
			if (mode == 2)
				insert(tabTmp, index1, index2, n, m);//wstawianie
			if (mode == 3)
				twist(tabTmp, index1, index2, m); //odwracanie

			currentCMax = CMax(tabTmp, n, m);
			deltaCMax = previousCMax - currentCMax;
			if (currentCMax < previousCMax)
				previousCMax = currentCMax;
			else
			{
				rnd = (rand() % 10000) / 10000.0;
				p = exp((float)deltaCMax / Temp);
				if (rnd < p)
					previousCMax = currentCMax;
				else
				{
					/*ruch powrotny*/
					if (mode == 1)
						swap(tabTmp, index2, index1, m);
					if (mode == 2)
						invInsert(tabTmp, index2, index1, n, m);
					if (mode == 3)
						twist(tabTmp, index1, index2, m);
				}

			}

		}
		Temp = Temp * wsp;

	}
	return CMax(tabTmp, n, m);
}

LARGE_INTEGER startTimer()//start odmierzania czasu
{
	LARGE_INTEGER start;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&start);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return start;
}

LARGE_INTEGER endTimer()//zakonczenie odmierzania czasu
{
	LARGE_INTEGER stop;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&stop);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return stop;
}

int main()
{
	//Zmienne do mierzenia czasu
	LARGE_INTEGER performanceCountStart, performanceCountEnd;
	__int64 freq;
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
	double stepSA_Time, SA_Time;

	srand(time(NULL));
	Work work; // pojedyncze zadanie
	int n = 0; //ilosc zadan
	int m = 0; //ilosc maszyn
	int p = 0; //czas trwania zadania
	int nrM = 0;//numer maszyny
	int stepCmax = 0;
	int mode = 0;
	int ilosc_powtorzen = 3; //do testów 30//
	//int NEH_value, NEHMod_value;
	string name = "data.txt", dataName;
	fstream plik;
	plik.open(name.c_str());

	for (int k = 0; k < N; k++)//petla wlasciwego dzialania programu
	{
		Work ** tab;
		plik >> dataName >> n >> m;
		tab = new Work*[n];
		for (int i = 0; i < n; i++)
		{
			tab[i] = new Work[m];
			for (int j = 0; j < m; j++)
			{
				plik >> nrM >> p;
				work = Work(p, i + 1, nrM);
				tab[i][j] = work;
			}
		}

		
		if(true)
		{
			for (int l = 1; l < 4; l++) 
			{
				/*MODE = 1 -> swap */
				/*MODE = 2 -> insert */
				/*MODE = 3 -> twist */
				mode = l;
				float sum = 0;
				SA_Time = 0;
				for (int a = 0; a < ilosc_powtorzen; a++) /*Podczas testów -> 30 powtórzeń*/
				{
					Work ** tabTemp;
					tabTemp = new Work*[n];
					for (int i = 0; i < n; i++)
					{
						tabTemp[i] = new Work[m];
						for (int j = 0; j < m; j++)
							tabTemp[i][j] = tab[i][j];
					}

					performanceCountStart = startTimer();//start timera
					stepCmax = SA(tab, tabTemp, n, m, CMax(tab, n, m), mode);
					performanceCountEnd = endTimer(); //stop timera
					stepSA_Time = 1000 * (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
					SA_Time += stepSA_Time;

					sum += (float)stepCmax;
					/*cout << "stepCmax: " << stepCmax << endl;
					cout << "stepSA_Time: " << stepSA_Time << endl;*/

					for (int i = 0; i < n; i++)
						delete[] tabTemp[i];
					delete[] tabTemp;
				}

				if (mode == 1)
					cout << "**********SWAP*********" << endl;
				if (mode == 2)
					cout << "*********INSERT********" << endl;
				if (mode == 3)
					cout << "*********TWIST*********" << endl;
				cout << "Sredni Cmax " << k + 1 << ":  " << sum / ilosc_powtorzen << endl;
				cout << "Sredni czas SA " << k + 1 << ":  " << SA_Time / ilosc_powtorzen << " ms\n\n";
			}
			cout << "\n***********************************\n";
			cout << "***********************************\n\n";
		}
		
		//usuwanie tablicy dynamicznej
		for (int i = 0; i < n; i++)
			delete[] tab[i];
		delete[] tab;
		
		
	}
	plik.close();
	system("pause");
	exit(0);
}
