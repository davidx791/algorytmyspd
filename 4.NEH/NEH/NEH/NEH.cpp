﻿// NEH.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>		
#include <iostream>		
#include <fstream>		
#include <string>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <ctime>
#include <Windows.h>

#define MAX 99999999	// Duza liczba do porownan
#define BASE 2	//Baza do potegowania
#define N 50 //liczba plikow

using namespace std;

class Work {//klasa przechowujaca informacje o jednym konkretnym zadaniu

private:
	int p; //czas trwania zadania
	int nr; //nr zadania
	int nrM;//nr maszyny
	int T;//czas zakonczenia

public:
	Work() { p = 0, nr = 0, nrM = 0; T = 0; } //konstruktor pusty
	Work(int pp, int nrr, int nrrM)//konstruktor z parametrami
	{
		p = pp;
		nr = nrr;
		nrM = nrrM;
		T = 0;
	}

	//funkcje odpowiedzialne za odczyt danych prywatnych
	int GetP() { return p; }
	int GetNr() { return nr; }
	int GetNrM() { return nrM; }
	int GetT() { return T; }

	void ChangeP(int pp) { p = pp; }
	void ChangeNr(int nrr) { nr = nrr; }
	void ChangeNrM(int nrrM) { nrM = nrrM; }
	void ChangeT(int TT) { T = TT; }

};

class Sum {//klasa przechowujaca informacje sumie wszystkich operacji zadania

private:
	int prio; //suma wszystkich operacji zadania
	int nr; //nr zadania

public:
	Sum() { prio = 0, nr = 0; } //konstruktor pusty
	Sum(int pprio, int nrr)//konstruktor z parametrami
	{
		prio = pprio;
		nr = nrr;
	}

	//funkcje odpowiedzialne za odczyt danych prywatnych
	int GetPrio() { return prio; }
	int GetNr() { return nr; }
};

bool prioSort(Sum x, Sum y)
{
	return (x.GetPrio() > y.GetPrio());
}

int CMax(Work **tab, int n, int m)
{
	int CMax = 0;
	int tmpT = 0;
	int j = 0;
	for (int i = 0; i < n; i++)
	{

		if (tab[i][j].GetNrM() == 1)
		{
			if (i > 0)
				tab[i][j].ChangeT(tab[i][j].GetP() + tab[i - 1][j].GetT());
			else
				tab[i][j].ChangeT(tab[i][j].GetP());
		}
		else
		{
			if (i > 0)
			{
				tmpT = max(tab[i][j].GetP() + tab[i][j - 1].GetT(), tab[i][j].GetP() + tab[i - 1][j].GetT());
				tab[i][j].ChangeT(tmpT);
			}
			else
			{
				tmpT = max(tab[i][j].GetP() + tab[i][j - 1].GetT(), tab[i][j].GetP());
				tab[i][j].ChangeT(tmpT);
			}
		}
		//cout << tab[i][j].GetT() << " ";
		if (i == n - 1)
		{
			j++;
			i = -1;
			if (j == m)
			{
				//cout << "\n";
				break;
			}
		}
	}
	CMax = tab[n - 1][m - 1].GetT();
	return CMax;
}


int NEH(Work **tab, Work **tabTmp, int n, int m)
{
	int min = MAX, mintmp;
	Work work; // pojedyncze zadanie
	vector<Sum>allSum; //vector wszystkich wierszy dla danej permutacji z wyliczonymi sumami
	Sum sum; //pojedynczy wiersz

	int **tabPrio = new int*[n]; //tablica przechowytujaca sumy w(j) i nr wiersza
	int *tabNr = new int[n]; //tablica kolejnosci wierszow

	for (int i = 0; i < n; i++) //wyznaczanie sum
	{
		tabPrio[i] = new int[2];
		tabPrio[i][0] = 0;
		tabPrio[i][1] = i + 1;
		for (int j = 0; j < m; j++)
			tabPrio[i][0] += tab[i][j].GetP();
		sum = Sum(tabPrio[i][0], tabPrio[i][1]);

		allSum.push_back(sum); //dodawanie do wektora sum
	}

	stable_sort(allSum.begin(), allSum.end(), prioSort); //sortowanie malejace po sumach

	for (int i = 0; i < n; i++) //utworzenie tablicy kolejnosci
	{
		tabNr[i] = allSum[i].GetNr();
		//cout << tabNr[i] << " ";
	}
	//cout << "\n";
	for (int i = 0; i < n; i++) //przypisanie do tabTmp kolejnych wierszy wg tablicy kolejnosci
	{
		for (int j = 0; j < m; j++)
		{
			tabTmp[i][j].ChangeNrM(tab[tabNr[i] - 1][j].GetNrM());
			tabTmp[i][j].ChangeP(tab[tabNr[i] - 1][j].GetP());
		}
	}

	//****sortowanie wg sum, ulozenie malejaco, dane w tablicy tab
	tab = tabTmp;

	for (int i = 0; i < 2; i++)
		delete[] tabPrio[i];
	delete[] tabPrio;
	delete[] tabNr;

	for (int i = 0; i < n; i++) //krok 3. petla dla kazdego nowego zadania
	{
		Work **tabTmp2; // tablica do sprawdzania Cmaxów
		tabTmp2 = new Work*[i + 1]; //rozmiar zwieksza sie dla kazdego zadania
		for (int x = 0; x < i + 1; x++) //wpisanie danych do tablicy tabTmp2 podstawowej bez zamian
		{								//nowe zadania na koncu
			tabTmp2[x] = new Work[m];
			for (int y = 0; y < m; y++)
			{
				work = Work(tab[x][y].GetP(), y + 1, tab[x][y].GetNrM());
				tabTmp2[x][y] = work;
			}
		}

		min = CMax(tabTmp2, i + 1, m); // wyliczenie pierwszego Cmaxa w danej petli
		for (int j = 0; j < i; j++)
		{
			Work tmp;
			for (int y = 0; y < m; y++) //zamiana nowego zadania z poprzednimi i sprawdzanie Cmaxa
			{
				tmp = tabTmp2[i - j][y];
				tabTmp2[i - j][y] = tabTmp2[i - j - 1][y];
				tabTmp2[i - j - 1][y] = tmp;
			}

			mintmp = CMax(tabTmp2, i + 1, m); // wyliczenie kolejynch Cmaxów 
			if (mintmp <= min) //podmiana poczatkowej czesci tablicy wykorzystywanej przy kolejnym wczytywaniu
			{
				min = mintmp;
				for (int x = 0; x < i + 1; x++)
					for (int y = 0; y < m; y++)
						tab[x][y] = tabTmp2[x][y];
			}
		}
		for (int x = 0; x < i + 1; x++)
			delete[] tabTmp2[x];
		delete[] tabTmp2;
		tabTmp2 = NULL;
	}
	return min;

}

int NEHMod(Work **tabMod, Work **tabTmpMod, int n, int m)
{
	int minMod = MAX, mintmpMod, minCMax;
	int xP;//czas trwania x
	int xIndex = 0;
	Work work; // pojedyncze zadanie
	Work *x;// do kroku 5
	vector<Sum>allSumMod; //vector wszystkich wierszy dla danej permutacji z wyliczonymi sumami
	Sum sum; //pojedynczy wiersz

	int **tabPrioMod = new int*[n]; //tablica przechowytujaca sumy w(j) i nr wiersza
	int *tabNrMod = new int[n]; //tablica kolejnosci wierszow

	for (int i = 0; i < n; i++) //wyznaczanie sum
	{
		tabPrioMod[i] = new int[2];
		tabPrioMod[i][0] = 0;
		tabPrioMod[i][1] = i + 1;
		for (int j = 0; j < m; j++)
			tabPrioMod[i][0] += tabMod[i][j].GetP();
		sum = Sum(tabPrioMod[i][0], tabPrioMod[i][1]);

		allSumMod.push_back(sum); //dodawanie do wektora sum
	}

	stable_sort(allSumMod.begin(), allSumMod.end(), prioSort); //sortowanie malejace po sumach

	for (int i = 0; i < n; i++) //utworzenie tablicy kolejnosci
	{
		tabNrMod[i] = allSumMod[i].GetNr();
		//cout << tabNrMod[i] << " ";
	}
	//cout << "\n";
	for (int i = 0; i < n; i++) //przypisanie do tabTmp kolejnych wierszy wg tablicy kolejnosci
	{
		for (int j = 0; j < m; j++)
		{
			tabTmpMod[i][j].ChangeNrM(tabMod[tabNrMod[i] - 1][j].GetNrM());
			tabTmpMod[i][j].ChangeP(tabMod[tabNrMod[i] - 1][j].GetP());
		}
	}

	//****sortowanie wg sum, ulozenie malejaco, dane w tablicy tabMod
	tabMod = tabTmpMod;

	for (int i = 0; i < 2; i++)
		delete[] tabPrioMod[i];
	delete[] tabPrioMod;
	delete[] tabNrMod;


	for (int i = 0; i < n; i++) //krok 3. petla dla kazdego nowego zadania
	{
		int licz = i;
		Work **tabTmp2Mod; // tablica do sprawdzania Cmaxów
		Work ** tabXMod;
		tabTmp2Mod = new Work*[i + 1]; //rozmiar zwieksza sie dla kazdego zadania
		tabXMod = new Work*[i + 1];
		for (int x = 0; x < i + 1; x++) //wpisanie danych do tablicy tabTmp2 podstawowej bez zamian
		{								//nowe zadania na koncu
			tabTmp2Mod[x] = new Work[m];
			tabXMod[x] = new Work[m];
			for (int y = 0; y < m; y++)
			{
				work = Work(tabMod[x][y].GetP(), y + 1, tabMod[x][y].GetNrM());
				tabTmp2Mod[x][y] = work;
				//cout << " " << tabTmp2Mod[x][y].GetNrM()<<" "<< tabTmp2Mod[x][y].GetP() << " ";
			}
			//cout << "\n";
		}
		//	cout << "\n";
		minMod = CMax(tabTmp2Mod, i + 1, m); // wyliczenie pierwszego Cmaxa w danej pêtli
		Work tmp;

		for (int j = 0; j < i; j++)
		{

			for (int y = 0; y < m; y++) //zamiana nowego zadania z poprzednimi i sprawdzanie Cmaxa
			{
				tmp = tabTmp2Mod[i - j][y];
				tabTmp2Mod[i - j][y] = tabTmp2Mod[i - j - 1][y];
				tabTmp2Mod[i - j - 1][y] = tmp;
			}

			mintmpMod = CMax(tabTmp2Mod, i + 1, m); // wyliczenie kolejynch Cmaxów 
			if (mintmpMod <= minMod) //podmiana poczatkowej czesci tablicy wykorzystywanej przy kolejnym wczytywaniu
			{
				licz = i - j - 1;
				minMod = mintmpMod;
				for (int x = 0; x < i + 1; x++)
					for (int y = 0; y < m; y++)
						tabMod[x][y] = tabTmp2Mod[x][y];
			}
		}
		

		for (int j = 0; j < i; j++)
		{
			for (int y = 0; y < m; y++) //zamiana nowego zadania z poprzednimi i sprawdzanie Cmaxa
			{

				xP = tabMod[i - j][y].GetP();
				tabMod[i - j][y].ChangeP(0);

				mintmpMod = CMax(tabMod, i + 1, m);
				tabMod[i - j][y].ChangeP(xP);
				if (minMod >= mintmpMod && licz != i - j)
				{
					minMod = mintmpMod;
					x = &tabMod[i - j][y];
					xIndex = i - j;
					//cout <<i<<" "<< xIndex << endl;
				}

			}

		}
		
		minCMax = CMax(tabMod, i + 1, m); // wyliczenie pierwszego Cmaxa w danej pêtli

		for (int x = 0; x < i + 1; x++)
			for (int y = 0; y < m; y++)
				tabXMod[x][y] = tabMod[x][y];

		for (int j = 0; j < i; j++)
		{

			for (int y = 0; y < m; y++) //zamiana nowego zadania z poprzednimi i sprawdzanie Cmaxa
			{
				tmp = tabXMod[xIndex][y];
				tabXMod[xIndex][y] = tabXMod[i - j][y];
				tabXMod[i - j][y] = tmp;
			}

			minCMax = CMax(tabXMod, i + 1, m); // wyliczenie kolejynch Cmaxów 
			if (minCMax <= minMod) //podmiana poczatkowej czesci tablicy wykorzystywanej przy kolejnym wczytywaniu
			{
				minMod = minCMax;
				for (int x = 0; x < i + 1; x++)
					for (int y = 0; y < m; y++)
						tabMod[x][y] = tabXMod[x][y];
			}
		}
		
		
	}

	return minMod;
}


LARGE_INTEGER startTimer()//start odmierzania czasu
{
	LARGE_INTEGER start;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&start);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return start;
}

LARGE_INTEGER endTimer()//zakonczenie odmierzania czasu
{
	LARGE_INTEGER stop;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&stop);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return stop;
}

int main()
{
	//Zmienne do mierzenia czasu
	LARGE_INTEGER performanceCountStart, performanceCountEnd;
	__int64 freq;
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
	double NEH_Time, NEHMod_Time;

	Work work; // pojedyncze zadanie
	int n = 0; //ilosc zadan
	int m = 0; //ilosc maszyn
	int p = 0; //czas trwania zadania
	int nrM = 0;//numer maszyny
	int NEH_value, NEHMod_value;
	string name = "data.txt", dataName;
	fstream plik;
	plik.open(name.c_str());


	for (int k = 0; k < N; k++)//petla wlasciwego dzialania programu
	{
		Work ** tabNEH, **tabNEHMod, **tabTmpNEH, **tabTmpNEHMod, **R, **Q;
		plik >> dataName >> n >> m;
		tabNEH = new Work*[n];
		tabTmpNEH = new Work*[n];

		for (int i = 0; i < n; i++)
		{
			tabNEH[i] = new Work[m];
			tabTmpNEH[i] = new Work[m];

			for (int j = 0; j < m; j++)
			{
				plik >> nrM >> p;
				work = Work(p, i + 1, nrM);
				tabNEH[i][j] = work;
			}
		}


		tabNEHMod = tabNEH; //skopiowanie tablic wejsciowych
		tabTmpNEHMod = tabTmpNEH;

		/*cout << "\nTablica wejsciowa NEH\n";
		for (int i = 0; i < n; i++) //wyswietlanie wczytanej tablicy
		{
		for (int j = 0; j < m; j++)
		cout << tabNEH[i][j].GetNrM() << " " << tabNEH[i][j].GetP() << " ";
		cout << "\n";
		}
		cout << "\nCMax wejsciowej tablicy " <<k+1<<": " << CMax(tabNEH, n, m) << "\n";
		cout << "\n*********************************\n";
		*/
		/*cout << "\nTablica wejsciowa NEHMod\n";
		for (int i = 0; i < n; i++) //wyswietlanie wczytanej tablicy
		{
		for (int j = 0; j < m; j++)
		cout << tabNEHMod[i][j].GetNrM() << " " << tabNEHMod[i][j].GetP() << " ";
		cout << "\n";
		}
		cout << "\nCMax wejsciowej tablicy " <<k+1<<": " << CMax(tabNEHMod, n, m) << "\n";
		cout << "\n*********************************\n";*/

		cout << "\n*********************************\n";
		performanceCountStart = startTimer();//start timera
		NEH_value = NEH(tabNEH, tabTmpNEH, n, m);
		cout << "\nCMax dla NEH " << k + 1 << ": " << NEH_value << "\n";
		performanceCountEnd = endTimer(); //stop timera
		NEH_Time = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
		cout << "Czas wykonywania NEH: " << 1000 * NEH_Time << " ms\n\n";

		performanceCountStart = startTimer();//start timera
		NEHMod_value = NEHMod(tabNEHMod, tabTmpNEHMod, n, m);
		cout << "\nCMax dla NEHMod " << k + 1 << ": " << NEHMod_value << "\n";
		performanceCountEnd = endTimer(); //stop timera
		NEHMod_Time = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
		cout << "Czas wykonywania NEHMod: " << 1000 * NEHMod_Time << " ms\n\n";
		cout << "\n*********************************\n";

		//usuwanie tablicy dynamicznej
		for (int i = 0; i < n; i++)
			delete[] tabNEH[i];
		delete[] tabNEH;
		for (int i = 0; i < n; i++)
			delete[] tabTmpNEH[i];
		delete[] tabTmpNEH;

	}
	plik.close();
	system("pause");
	exit(0);
}



