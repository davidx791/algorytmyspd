﻿// Carlier.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <Windows.h>

using namespace std;

class Work {//klasa przechowujaca infomracje o jednym konkretnym zadaniu
private:

	int p; //czas przygotowania
	int w; //czas wykonania
	int d; //czas dostarczenia
	int nr; //nr zadania
	int tOn;//czas zaczecia zadania

public:

	Work() { p = 0, w = 0, d = 0, nr = 0; tOn = 0; }//konstruktor pusty
	Work(int rr, int pp, int qq, int nrr)//wywolanie konstruktora z parametrami
	{
		p = rr;
		w = pp;
		d = qq;
		nr = nrr;
	}

	int GetR() { return p; }//funkcje odpowiedzialne za odczyt danych prywatnych
	int GetP() { return w; }
	int GetQ() { return d; }
	int GetNr() { return nr; }
	int GetTOn() { return tOn; }
	void ChangeR(int _r) { p = _r; }
	void ChangeP(int _p) { w = _p; }
	void ChangeQ(int _q) { d = _q; }
	void ChangeTOn(int _tOn) { tOn = _tOn; }

};

/*zmienne globalne*/
vector<Work> carlier;//uszeregowany carlier
int UB;
/*=================*/
int CMax(vector<Work> &allWork)//funkcja zliczajaca CMax, tak zwana funkcja celu
{
	int CMax = 0;
	int i = 0;
	int time = 0;
	int size = allWork.size();
	while (i < size)
	{
		if (allWork[i].GetR() <= time)
		{
			//uwzglêdnienie czasu wykonania
			time = time + allWork[i].GetP();
			//uwzglêdnienie czasu dostarczenia
			if (CMax <= time + allWork[i].GetQ())
			{
				CMax = time + allWork[i].GetQ();
			}
			i++;
		}
		else
			time++; //uwzglêdnienie czasu przygotowania
	}
	return CMax;
}

void Rsort(vector<Work> &allWork, int left, int right) {//sortowanie na podstawie parametru R, wykorzystywany jest quicksort

	int i = left;
	int j = right;
	Work temp;
	int pivot = allWork[(left + right) / 2].GetR();

	do {
		while (allWork[i].GetR() < pivot) i++;
		while (allWork[j].GetR() > pivot) j--;
		if (i <= j) {

			temp = allWork[i];

			allWork[i] = allWork[j];
			i++;

			allWork[j] = temp;
			j--;

		}
	} while (i <= j);

	if (left < j) Rsort(allWork, left, j);
	if (right > i) Rsort(allWork, i, right);

}

void Qsort(vector<Work> &allWork, int left, int right) {//sortowanie na podstawie parametru Q, wykorzystywany jest quicksort

	int i = left;
	int j = right;
	Work temp;
	int pivot = allWork[(left + right) / 2].GetQ();

	do {
		while (allWork[i].GetQ() > pivot) i++;
		while (allWork[j].GetQ() < pivot) j--;
		if (i <= j) {

			temp = allWork[i];

			allWork[i] = allWork[j];
			i++;

			allWork[j] = temp;
			j--;

		}
	} while (i <= j);

	if (left < j) Qsort(allWork, left, j);
	if (right > i) Qsort(allWork, i, right);

}



void _2opt(vector<Work> &allWork) {//funkcja algorytmu 2-opt

	int previousCmax, presentCmax;
	vector<Work>tempWork = allWork;
	int size = allWork.size();

	previousCmax = CMax(allWork);

	for (int i = 0; i < size - 1; i++)
	{

		for (int j = i + 1; j < size; j++)
		{

			tempWork[i] = allWork[j];//zamiana miejscami dwoch zadan
			tempWork[j] = allWork[i];
			presentCmax = CMax(tempWork);//wyliczanie CMax dla aktualnego ukladu zadan

			if (presentCmax < previousCmax)//jezeli po zamianie doszlo do polepszenia to zostawiamy to i startujemy o 0
			{
				previousCmax = presentCmax;
				allWork[i] = tempWork[i];
				allWork[j] = tempWork[j];
				i = 0;
				j = i + 1;

			}
			else
			{
				tempWork[i] = allWork[i];
				tempWork[j] = allWork[j];
			}
		}
	}
}

int Schrage(vector<Work> &allWork)
{
	vector<Work> G; //zbiór zadañ gotowych do realizacji
	vector<Work> tmp;
	vector<Work> N = allWork; // wektor zadañ uszeregowanych po R
	Work e;
	int Cmax = 0;
	int t = 0;
	int k = 0;
	int size = N.size();
	while (!N.empty() || !G.empty())
	{
		while (!N.empty() && N[0].GetR() <= t)
		{
			e = N[0];
			G.push_back(e); //dodawanie do wektora zadañ gotowych
			N.erase(N.begin()); //usuwanie z wektora uszeregowanych po R
		}

		if (G.empty())
			t = N[0].GetR(); // ustawienie czasu na najmniejsze R
		else
		{
			Qsort(G, 0, G.size() - 1); //sortowanie po Q malej¹co
			e = G[0];
			tmp.push_back(e); //dodajemy zadanie gotowe do wektora tmp
							  //tmp[0].ChangeTOn(t);
			G.erase(G.begin()); //usuwamy z wektora G 
			t += e.GetP(); //aktualizacja czasu - moment zakonczenia zadania
			if (Cmax < t + tmp[tmp.size() - 1].GetQ())
				Cmax = t + tmp[tmp.size() - 1].GetQ();
			tmp[tmp.size() - 1].ChangeTOn(t);
		}
	}
	allWork = tmp; //gotowy i posrtowany wektor
	return Cmax;
}


int PrmtSchrage(vector<Work> &allWork)
{
	vector<Work> G;//pusty vector G do którego bêdziemy oddawaæ poszczególne prace i sortowaæ je po Q
	vector<Work> N = allWork;//vector N z posortowanymi po R pracami
							 //vector<Work> tmp;
	Work e, l;//e - praca wybrana, l - aktualnie wykonywana praca
	int CMax = 0;//wyliczona funkcja celu
	int t = 0;//czas
	int size = N.size();//ilosc prac

	while (!N.empty() || !G.empty())
	{
		while (!N.empty() && N[0].GetR() <= t)
		{
			e = N[0];
			G.push_back(e); //dodawanie zadania do wektora G
			N.erase(N.begin()); //usuwanie go z N
								//cout << l.GetQ() << "\n";
								//cout << e.GetR() << "\n"; 
			if (e.GetQ() > l.GetQ()) //czy e ma wiêkszy czas dostarczenia od l
			{
				//int newP = t - e.GetR();
				//cout << t << "\n";
				//cout << newP << "\n";
				//przerwanie zadania 
				l.ChangeP(t - e.GetR()); //aktualizacja P
				t = e.GetR();

				if (l.GetP() > 0) //wrzucenie pozosta³ej czêœci l do wektora G
				{
					G.push_back(l);
				}
			}

		}

		if (G.empty())
			t = N[0].GetR();
		else
		{
			Qsort(G, 0, G.size() - 1); //sortowanie malej¹ce po Q
			e = G[0];
			//tmp.push_back(e);
			G.erase(G.begin()); //usuwanie zadanie z wektora G
		}

		l = e; //uaktualnienie zmiennej l
		t = t + e.GetP(); //aktualizacja czasu - moment zakonczenia zadania

		if (CMax >= (t + e.GetQ())) //wyliczanie CMax
		{
			CMax = CMax;
		}
		else
			CMax = t + e.GetQ();
	}

	//cout << tmp.size() << "\n";
	return CMax;
}

void Carlier(vector<Work> allWork)
{
	int _r = INT_MAX, _p = 0, _q = INT_MAX;
	int a = 0, b = 0, c = 0;
	int oldR = 0, oldQ = 0;//zmienne potrzebne do odtworzenia prac
	int LB = 0, U = 0;//granice
	int suma = 0;//suma wykorzystywana przy wyliczaniu a
	int size = allWork.size();//ilosc zadan

	U = Schrage(allWork);

	if (U <= UB)
	{
		UB = U;
		carlier = allWork;
	}

	//Obliczanie sciezki krytycznej
	//cout << U << "\n";
	//wyliczanie b
	for (int i = size - 1; i > 0; i--)
	{
		if (allWork[i].GetTOn() + allWork[i].GetQ() == U)
		{
			b = i;
			break;
		}
	}
	
	//wyliczanie a
	for (int i = 0; i < size; i++)
	{
		suma = 0;
		for (int j = i; j <= b; j++)
			suma += allWork[j].GetP();

		if (allWork[i].GetR() + suma + allWork[b].GetQ() == U)
		{
			a = i;
			break;
		}
	}

	//wyliczanie c
	c = 0;
	for (int i = b; i >= a; i--)
	{
		if (allWork[i].GetQ() < allWork[b].GetQ())
		{
			c = i;
			break;
		}
	}
	//cout << "Koncowe c: " << c << "\n";

	//cout << "===================" << "\n";

	if (c == 0)//koniec jezeli znaleziono opt
	{
		return;
	}

	for (int i = c + 1; i <= b; i++)
	{
		if (_r > allWork[i].GetR())
			_r = allWork[i].GetR();
		if (_q > allWork[i].GetQ())
			_q = allWork[i].GetQ();
		_p += allWork[i].GetP();
	}

	if (allWork[c].GetR() < _r + _p)
	{
		oldR = allWork[c].GetR();
		allWork[c].ChangeR(_r + _p);
	}

	LB = PrmtSchrage(allWork);
	//cout << "LB1: " << LB << "\n";
	//cout << "UB1: " << UB << "\n";
	if (LB < UB)
	{
		Carlier(allWork);
	}
	allWork[c].ChangeR(oldR);


	if (allWork[c].GetQ() < _q + _p)
	{
		oldQ = allWork[c].GetQ();
		allWork[c].ChangeQ(_q + _p);
	}

	LB = PrmtSchrage(allWork);
	//cout << "LB2: " << LB << "\n";
	//cout << "UB2: " << UB << "\n";
	if (LB < UB)
	{
		Carlier(allWork);
	}
	allWork[c].ChangeQ(oldQ);

}



LARGE_INTEGER startTimer()//start odmierzania czasu
{
	LARGE_INTEGER start;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&start);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return start;
}
LARGE_INTEGER endTimer()//zakonczenie odmierzania czasu
{
	LARGE_INTEGER stop;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&stop);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return stop;
}


int main()
{
	//******Zmienne do mierzenia czasu******//
	LARGE_INTEGER performanceCountStart, performanceCountEnd;
	__int64 freq;
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
	double Time;
	double RsortTime;
	double _2optTime;
	double _2opt_RsortTime;
	double _SchrageTime;
	double _PrmtSchrageTime;
	double _CarlierTime;

	//******Zmienne do zadania******//
	vector<Work>allWorkCarlier, allWorkR, allWorkPrmt; //wektor wszystkich zadañ
	Work work; // pojedyncze zadanie
	int n; //ilosc zadan
	int r; //czas przygotowania
	int p; //czas wykonania
	int q; //czas dostarczenia
	int param; //ilosc parametrow
	int Sum = 0;
	string name; //nazwa pliku
	ifstream plik;

	//***Wczytywanie zadan do wektora***//
	cout << "Podaj nazwe pliku z rozszerzeniem: \n";
	cin >> name;

	plik.open(name.c_str());
	plik >> n;
	plik >> param;
	for (int i = 0; i < n; i++)
	{
		plik >> r >> p >> q;
		work = Work(r, p, q, i + 1);
		allWorkR.push_back(work);
		//allWork2opt.push_back(work);
	}

	//***Cmax i czas zwyk³y***//
	/*performanceCountStart = startTimer();//czas poczatkowy
	CMax(allWorkR);
	performanceCountEnd = endTimer(); //czas koncowy
	Time = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "CMax: " << CMax(allWorkR) << "\n";
	cout << "Czas wykonywania: " << Time << "\n\n";*/

	//***Cmax i czas dla sortR***//
	//performanceCountStart = startTimer();//czas poczatkowy
	int cmax = CMax(allWorkR);
	cout << "Czysty CMax: " << cmax << "\n";
	Rsort(allWorkR, 0, n - 1);
	allWorkPrmt = allWorkR;
	allWorkCarlier = allWorkR;
	
	performanceCountStart = startTimer();//czas poczatkowy
	cout << "CMax dla  Schrage: " << Schrage(allWorkR) << "\n";
	performanceCountEnd = endTimer(); //czas koncowy
	_SchrageTime = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "Czas wykonywania Schrage: " << _SchrageTime << "\n\n";

	performanceCountStart = startTimer();//czas poczatkowy
	cout << "CMax dla  PrmtSchrage: " << PrmtSchrage(allWorkPrmt) << "\n";
	performanceCountEnd = endTimer(); //czas koncowy
	_PrmtSchrageTime = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "Czas wykonywania PrmtSchrage: " << _PrmtSchrageTime << "\n\n";

	performanceCountStart = startTimer();//czas poczatkowy
	UB = cmax;
	Carlier(allWorkCarlier);
	cout << "CMax dla  Carlier: " << CMax(carlier) << "\n";
	performanceCountEnd = endTimer(); //czas koncowy
	_CarlierTime = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "Czas wykonywania Carlier: " << _CarlierTime << "\n\n";
	
	//PrmtSchrage(allWorkPrmt);
	//cout << "CMax dla  PrmtSchrage: " << CMax(allWorkPrmt) << "\n";

	/*performanceCountEnd = endTimer(); //czas koncowy
	RsortTime = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "CMax dla Rsort: " << CMax(allWorkR) << "\n";
	cout << "Czas wykonywania Rsort: " << RsortTime << "\n\n";*/

	//***Cmax dla czas dla 2-opt***//
	/*performanceCountStart = startTimer();//czas poczatkowy
	_2opt(allWork2opt);
	performanceCountEnd = endTimer(); //czas koncowy
	_2optTime = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "CMax czas dla 2opt: " << CMax(allWork2opt) << "\n";
	cout << "Czas wykonywania 2opt: " << _2optTime << "\n\n";*/

	//***Cmax i czas dla R + 2-opt **//
	/*performanceCountStart = startTimer();//czas poczatkowy
	_2opt(allWorkR);
	performanceCountEnd = endTimer(); //czas koncowy
	_2opt_RsortTime = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "CMax czas dla sortR i 2opt: " << CMax(allWorkR) << "\n";
	cout << "Czas wykonywania sort R i 2opt: " << _2opt_RsortTime << "\n\n";*/


	system("pause");
	return 0;
}
