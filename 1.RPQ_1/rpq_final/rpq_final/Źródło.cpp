// rpq_final.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>
#include <string>
#include <cstdlib>
#include <Windows.h>


using namespace std;

class Work {//klasa przechowujaca infomracje o jednym konkretnym zadaniu
private:

	int r; //czas przygotowania
	int p; //czas wykonania
	int q; //czas dostarczenia
	int nr; //nr zadania

public:

	Work() { r = 0, p = 0, q = 0, nr = 0; }//konstruktor pusty
	Work(int rr, int pp, int qq, int nrr)//wywolanie konstruktora z parametrami
	{
		r = rr;
		p = pp;
		q = qq;
		nr = nrr;
	}

	int GetR() { return r; }//funkcje odpowiedzialne za odczyt danych prywatnych
	int GetP() { return p; }
	int GetQ() { return q; }
	int GetNr() { return nr; }

};

int CMax(vector<Work> &allWork)//funkcja zliczajaca CMax, tak zwana funkcja celu
{
	int CMax = 0;
	int i = 0;
	int time = 0;
	int size = allWork.size();
	while (i < size)
	{
		if (allWork[i].GetR() <= time)
		{
			//uwzględnienie czasu wykonania
			time = time + allWork[i].GetP();
			//uwzględnienie czasu dostarczenia
			if (CMax <= time + allWork[i].GetQ())
			{
				CMax = time + allWork[i].GetQ();
			}
			i++;
		}
		else
			time++; //uwzględnienie czasu przygotowania
	}
	return CMax;
}

void Rsort(vector<Work> &allWork, int left, int right) {//sortowanie na podstawie parametru R, wykorzystywany jest quicksort

	int i = left;
	int j = right;
	Work temp;
	int pivot = allWork[(left + right) / 2].GetR();

	do {
		while (allWork[i].GetR() < pivot) i++;
		while (allWork[j].GetR() > pivot) j--;
		if (i <= j) {

			temp = allWork[i];

			allWork[i] = allWork[j];
			i++;

			allWork[j] = temp;
			j--;

		}
	} while (i <= j);

	if (left < j) Rsort(allWork, left, j);
	if (right > i) Rsort(allWork, i, right);

}



void _2opt(vector<Work> &allWork) {//funkcja algorytmu 2-opt

	int previousCmax, presentCmax;
	vector<Work>tempWork = allWork;
	int size = allWork.size();

	previousCmax = CMax(allWork);

	for (int i = 0; i < size - 1; i++)
	{

		for (int j = i + 1; j < size; j++)
		{

			tempWork[i] = allWork[j];//zamiana miejscami dwoch zadan
			tempWork[j] = allWork[i];
			presentCmax = CMax(tempWork);//wyliczanie CMax dla aktualnego ukladu zadan

			if (presentCmax < previousCmax)//jezeli po zamianie doszlo do polepszenia to zostawiamy to i startujemy o 0
			{
				previousCmax = presentCmax;
				allWork[i] = tempWork[i];
				allWork[j] = tempWork[j];
				i = 0;
				j = i + 1;

			}
			else
			{
				tempWork[i] = allWork[i];
				tempWork[j] = allWork[j];
			}
		}
	}
}

LARGE_INTEGER startTimer()//start odmierzania czasu
{
	LARGE_INTEGER start;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&start);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return start;
}
LARGE_INTEGER endTimer()//zakonczenie odmierzania czasu
{
	LARGE_INTEGER stop;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
	QueryPerformanceCounter(&stop);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
	return stop;
}


int main()
{
	//******Zmienne do mierzenia czasu******//
	LARGE_INTEGER performanceCountStart, performanceCountEnd;
	__int64 freq;
	QueryPerformanceFrequency((LARGE_INTEGER *)&freq);
	double Time;
	double RsortTime;
	double _2optTime;
	double _2opt_RsortTime;

	//******Zmienne do zadania******//
	vector<Work>allWork2opt, allWorkR; //wektor wszystkich zadań
	Work work; // pojedyncze zadanie
	int n; //ilosc zadan
	int r; //czas przygotowania
	int p; //czas wykonania
	int q; //czas dostarczenia
	int param; //ilosc parametrow
	int Sum = 0;
	string name; //nazwa pliku
	ifstream plik;

	//***Wczytywanie zadan do wektora***//
	cout << "Podaj nazwe pliku z rozszerzeniem: \n";
	cin >> name;

	plik.open(name);
	plik >> n;
	plik >> param;
	for (int i = 0; i < n; i++)
	{
		plik >> r >> p >> q;
		work = Work(r, p, q, i + 1);
		allWorkR.push_back(work);
		allWork2opt.push_back(work);
	}
	//***Cmax i czas zwykły***//
	performanceCountStart = startTimer();//czas poczatkowy
	CMax(allWorkR);
	performanceCountEnd = endTimer(); //czas koncowy
	Time = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "CMax: " << CMax(allWorkR) << "\n";
	cout << "Czas wykonywania: " << Time << "\n\n";

	//***Cmax i czas dla sortR***//
	performanceCountStart = startTimer();//czas poczatkowy
	Rsort(allWorkR, 0, n - 1);
	performanceCountEnd = endTimer(); //czas koncowy
	RsortTime = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "CMax dla Rsort: " << CMax(allWorkR) << "\n";
	cout << "Czas wykonywania Rsort: " << RsortTime << "\n\n";

	//***Cmax dla czas dla 2-opt***//
	performanceCountStart = startTimer();//czas poczatkowy
	_2opt(allWork2opt);
	performanceCountEnd = endTimer(); //czas koncowy
	_2optTime = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "CMax czas dla 2opt: " << CMax(allWork2opt) << "\n";
	cout << "Czas wykonywania 2opt: " << _2optTime << "\n\n";

	//***Cmax i czas dla R + 2-opt **//
	performanceCountStart = startTimer();//czas poczatkowy
	_2opt(allWorkR);
	performanceCountEnd = endTimer(); //czas koncowy
	_2opt_RsortTime = (double)(performanceCountEnd.QuadPart - performanceCountStart.QuadPart) / freq;
	cout << "CMax czas dla sortR i 2opt: " << CMax(allWorkR) << "\n";
	cout << "Czas wykonywania sort R i 2opt: " << _2opt_RsortTime << "\n\n";

	system("pause");
	return 0;
}
